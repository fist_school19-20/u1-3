﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //private
    bool done = false;
    public Text label;
    public Text scoreText;
    public GameObject landscape;
    public GameObject finish; //null
    public int score = 0;
    int total = 0;

    void Start()
    {
        Teapot[] teapots = FindObjectsOfType<Teapot>();
        for (int i = 0; i < teapots.Length; i++)
        {
            total += teapots[i].cost;
        }
        ChangeScore(0);
        print(SceneManager.GetActiveScene().buildIndex);
    }


    void OnCollisionEnter(Collision collision)
    {
        if (done == false && collision.collider.gameObject == landscape)
        {
            done = true;
            // change text
            label.enabled = true;
            //Destroy(gameObject);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (!done && 
            other.gameObject == finish
            //&& score == total
            )
        {
            done = true;
            label.enabled = true;
            label.text = "win";
            label.color = Color.green;

            FindObjectOfType<GameManager>().playerScore += score;
            print(FindObjectOfType<GameManager>().playerScore);

            FindObjectOfType<GameManager>().LoadLevel(
                SceneManager.GetActiveScene().buildIndex + 1
                );
        }       
    }
    // <модиф. доступа>
    // <тип. возвр. знач.> 
    // <имя метода>
    // (<набор вх аргументов>)
    // { тело метода, блок операций }
    // void - формальный, пустой результат
    public void ChangeScore(int value)
    {
        score += value;
        scoreText.text = "Score: " + score + " | " + total;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            FindObjectOfType<GameManager>().LoadLevel(
                SceneManager.GetActiveScene().buildIndex);
        }
    }
}
