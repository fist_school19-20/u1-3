﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    public override void Attack(RaycastHit target)
    {
        base.Attack(target);
        GetComponent<Animator>().SetTrigger("attack");
        target.collider?.GetComponent<HP>()?.GetDamage(Damage);
    }

}
