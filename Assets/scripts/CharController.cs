﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class CharController : MonoBehaviour
{
    public RectTransform selector;
    public RectTransform rightPanel;
    public RectTransform invPanel;
    FirstPersonController fpsc;
    UIManager uim;

    public RectTransform content;
    public GameObject slotPrefab;
    public Sprite[] sprites;

    public LandscapeGenerator lg;
    int selectedBlock = 0;
    public Text hint;
    Camera cam;
    RaycastHit hit = new RaycastHit();
    public Transform weaponContainer;
    public RectTransform inventoryBar;
    public Transform phantomBlock;

    int[] resources = new int[4];
    int[] blocks = new int[4];
    void Start()
    {
        cam = GetComponentInChildren<Camera>();
        fpsc = GetComponent<FirstPersonController>();
        uim = invPanel.GetComponent<UIManager>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            // show inv
            bool target = !invPanel.gameObject.activeInHierarchy;
            invPanel.gameObject.SetActive(target);


            fpsc.enabled = !target;
            Cursor.visible = target;

            Time.timeScale = target == true ? 0 : 1;
            Cursor.lockState = target ? CursorLockMode.None : CursorLockMode.Locked;
            /*
            if (target == true)
            {
                Cursor.lockState = CursorLockMode.None;
                Time.timeScale = 0;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Time.timeScale = 1;
            }
            */
            if (target)
            {
                for (int i = 0; i < resources.Length; i++) // тип ресурсов
                {
                    for (int j = 0; j < resources[i]; j++) // количество i-го ресурса
                    {
                        GameObject s = GameObject.Instantiate(slotPrefab, content);
                        s.transform.GetChild(0).GetComponent<Image>().sprite =
                            sprites[i];
                        s.transform.GetChild(0).GetComponent<Resource>().Id = i;
                    }
                }
                int n = content.childCount;
                int cols = (int)((content.rect.size.x - 4) / 68);
                int rows = (n / cols) + 1;
                content.sizeDelta = new Vector2(
                    content.sizeDelta.x,
                    rows*64 + (rows+1)*4
                    );
            }
            else
            {
                foreach (Transform slot in content)
                {
                    Destroy(slot.gameObject);
                }
#warning clear craft table too
                uim.ClearTable();
                /*
                for (int i = 0; i < content.childCount; i++)
                {
                    Destroy(content.GetChild(i).gameObject);
                }
                */
            }

        }
        if (invPanel.gameObject.activeInHierarchy)
            return;


        Physics.Raycast(
            cam.transform.position,// точка начала
            cam.transform.forward,// вектор направления
            out hit,// выходная переменная, результат рейкаста
            4.0f// расстояние взаимодействия
            );
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.GetComponent<Weapon>() != null)
                hint.text = "press E to use " +
                    hit.collider.GetComponent<Weapon>()
                    .Name;
            else
                hint.text = "THIS IS " + hit.collider.gameObject.name;


            if (hit.collider.gameObject.tag == "Block")
            {
                // передвинуть фантом
                phantomBlock.GetComponent<MeshRenderer>().enabled = true;
                phantomBlock.position = hit.collider.transform.position +
                    hit.normal;
            }
            else
            {
                phantomBlock.GetComponent<MeshRenderer>().enabled = false;
            }
        }
        else
        {
            hint.text = "";
            phantomBlock.GetComponent<MeshRenderer>().enabled = false;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (hit.collider != null 
                && hit.collider.gameObject.GetComponent<Weapon>())
            {       // transform.SetParent(null) // tr -> scene
                DropWeapon();

                hit.collider.transform.SetParent(weaponContainer); // new weapon attach to wC
                hit.collider.transform.localPosition = Vector3.zero; // new weapon pos -> weaponcont pos
                weaponContainer.GetChild(0).localRotation = Quaternion.identity; // new weapon rot -> weapontCont rot
                weaponContainer.GetChild(0).GetComponent<Rigidbody>().isKinematic = true;
                weaponContainer.GetChild(0).GetComponent<Animator>().enabled = true;
            }
            if (hit.collider != null && hit.collider.gameObject.GetComponent<PieceOfBlock>())
            {
                ChangeMats(hit.collider.gameObject.GetComponent<PieceOfBlock>().Id, 1);
                //resources[hit.collider.gameObject.GetComponent<Block>().Id]++;
                hit.collider.gameObject.GetComponent<PieceOfBlock>().Get();
            }
        }
        if (Input.GetMouseButtonDown(0) && weaponContainer.childCount > 0)
        {
            // объектно-ориентир программирование
            // полиморфизм / Стратегия
            weaponContainer.GetChild(0).GetComponent<Weapon>()?.Attack(hit);
            /*
            RangeWeapon rw;
            MeleeWeapon mw;
            if ((rw = weaponContainer.GetChild(0).GetComponent<RangeWeapon>()) != null)
            {
                rw.Attack(hit);
            }
            if ((mw = weaponContainer.GetChild(0).GetComponent<MeleeWeapon>()) != null)
            {
                mw.Attack(hit);
            }
            */
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            weaponContainer.GetChild(0)?.GetComponent<RangeWeapon>()?.Reload(); // ? - Элвис-опретор, продолж выполн если не null
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            DropWeapon();
        }
        if (Input.GetMouseButtonDown(1))
        {
            //build block;
            GameObject.Instantiate(
                lg.prefabs[selectedBlock],
                phantomBlock.position,
                phantomBlock.rotation,
                lg.transform
                );
        }
        if (Input.GetKeyDown(KeyCode.Alpha1)) selectedBlock = 0;
        if (Input.GetKeyDown(KeyCode.Alpha2)) selectedBlock = 1;
        if (Input.GetKeyDown(KeyCode.Alpha3)) selectedBlock = 2;
        if (Input.GetKeyDown(KeyCode.Alpha4)) selectedBlock = 3;

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            selectedBlock += System.Math.Sign(Input.GetAxis("Mouse ScrollWheel"));
            
            int k = lg.prefabs.Length;
            selectedBlock = (k+selectedBlock) % k;

            selector.SetParent(rightPanel.GetChild(selectedBlock));
            selector.localPosition = Vector3.zero;
        }
    }
    void DropWeapon()
    {
        if (weaponContainer.childCount == 0)
            return;
        weaponContainer.GetChild(0).GetComponent<Rigidbody>().isKinematic = false;
        weaponContainer.GetChild(0).GetComponent<Animator>().enabled = false;
        weaponContainer.GetChild(0).SetParent(null);
    }
    public void ChangeMats(int id, int value)
    {
        resources[id] += value;
        inventoryBar.GetChild(id).GetComponent<Text>()
            .text = resources[id].ToString();
    }
    public void ChangeBlocks(int id, int value)
    {
        blocks[id] += value;
        rightPanel
            .GetChild(id) // panel
            .GetChild(0) // text go
            .GetComponent<Text>()
            .text = blocks[id].ToString();
    }
}
