﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public RectTransform[] craftSlots;
    public void Craft()
    {
        if (craftSlots[0].childCount == 0)
        {
            print("error!");
            return;
        }
        int id = craftSlots[0]
            .GetChild(0)
            .GetComponent<Resource>()
            .Id;
        foreach (Transform slot in craftSlots)
        {
            if (slot.childCount == 0 || slot.GetChild(0).GetComponent<Resource>().Id != id)
            {
                print("error!");
                return;
            }
        }
        GameManager.Instance().player.ChangeBlocks(id, 1);
        GameManager.Instance().player.ChangeMats(id, -4);
        ClearTable();
    }
    public void ClearTable()
    {
        foreach (RectTransform cs in craftSlots)
        {
            if (cs.childCount > 0)
                Destroy(cs.GetChild(0).gameObject);
        }
    }
}
