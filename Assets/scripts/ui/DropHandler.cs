﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DropHandler : MonoBehaviour,
    IDropHandler
{
    public void OnDrop(PointerEventData data)
    {
        if (data.pointerDrag.GetComponent<DragHandler>() != null)
        {
            data.pointerDrag.GetComponent<DragHandler>()
                .startParent = transform;
        }
    }
}
