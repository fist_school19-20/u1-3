﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour,
    IBeginDragHandler,
    IDragHandler,
    IEndDragHandler
{
    public Transform startParent;
    public void OnBeginDrag(PointerEventData data)
    {
        startParent = transform.parent;
        transform.SetParent(
            GetComponentInParent<UIManager>().transform
        ); // вложить в Inventory
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }
    public void OnDrag(PointerEventData data)
    {
        transform.position = data.position;
    }
    public void OnEndDrag(PointerEventData data)
    {
        transform.SetParent(startParent);
        transform.localPosition = Vector3.zero;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }
}
