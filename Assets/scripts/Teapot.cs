﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teapot : MonoBehaviour
{
    public int cost;
    void OnTriggerEnter(Collider other)
    {
        // GetComponent<T>()- получение компонента типа Т
        if (other.GetComponent<PlayerController>() != null)
        {
            //GetComponent<SphereCollider>().enabled = false; // отключ компонента
            Destroy(GetComponent<SphereCollider>()); // уничтожение компонента
            other
                .GetComponent<PlayerController>()
                .ChangeScore(cost);
            GetComponent<Animator>().Play("collect");
            Destroy(gameObject, 1f);
        }
        // вызов метода:
        // <имя метода> (значения аргументов);
    }
}
