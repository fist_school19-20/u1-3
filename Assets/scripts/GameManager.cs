﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
using System.Linq;
public class GameManager : MonoBehaviour
{
    public InputField pName;
    public int playerScore;
    public string currentPlayer;

    public CharController player;
    [SerializeField] Text killsText;
    int kills = 0;
    public List<Enemy> enemies;

    public UIManager uim;
    public LandscapeGenerator lg;

    [Header("UI")]
    public Text ammoText;

    public string path;

    #region Singleton
    // singleton = одиночка
    private static GameManager _instance;
    public static GameManager Instance() { return _instance; }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        DontDestroyOnLoad(gameObject);
        path = Application.dataPath + "/save.txt";
    }
    #endregion Singleton
    private void Start()
    {
        enemies = new List<Enemy>();
        if (File.Exists(path))
            LoadWorld();
        else
            CreateWorld();
    }
    public void LoadLevel(int index)
    {
        if (index == 1)
        {
            currentPlayer = pName.text;
        }
        if (index < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(index);
        }
        else
        {
            SceneManager.LoadScene(0);
            File.AppendAllText(
                "D:\\temp\\save.txt",
                currentPlayer + " " + playerScore + "\n"
            );
            Destroy(gameObject);
        }
    }
    public void ExitGame()
    {
        print("exit");
        Application.Quit();
    }

    public void CreateWorld()
    {
        lg.Create();
    }
    public void LoadWorld()
    {
        List<string> lines = File.ReadLines(path).ToList();
        lg.Load(lines);
    }
    public void SaveWorld()
    {
        //string[] lines = new string[lg.transform.childCount];
        List<string> lines = new List<string>();

        foreach (Transform block in lg.transform)
        {
            string line = block.position.x + ";" +
                          block.position.y + ";" +
                          block.position.z + ";" +
                          block.GetComponent<Block>().Id + ";" +
                          block.GetComponent<HP>().GetHP();
            lines.Add(line);
        }
        File.WriteAllLines(path, lines);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {
            SaveWorld();
        }
    }
    public void SetKills(int k)
    {
        kills += k;
        killsText.text = "Frags: " + kills + " / Enemies: " + enemies.Count;
    }
}
