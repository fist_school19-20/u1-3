﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject skeletonPrefab;
    public float cooldown; // 10 sec

    private float timer;

    void Start()
    {
        timer = cooldown; // = 10 sec
    }
    void Update()
    {
        timer -= Time.deltaTime; // 9.995 sec
        if (timer <= 0)
        {
            GameObject sk = GameObject.Instantiate(skeletonPrefab, transform.position, transform.rotation);
            timer = cooldown;
        }
    }
}
